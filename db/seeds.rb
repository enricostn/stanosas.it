# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


Spree::Core::Engine.load_seed if defined?(Spree::Core)
Spree::Auth::Engine.load_seed if defined?(Spree::Auth)

store = Spree::Store.where(code: 'spree').first
store.name = 'Stano s.a.s.'
store.url = 'shop.stanosas.it'
store.mail_from_address = 'copisteria@stanosas.it'
store.cart_tax_country_iso = 'IT'
store.save!
